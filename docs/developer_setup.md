# Education pipeline scraper

##
This describes how to install the jobed-connect pipeline (i.e. in the scraping-pipelin repo) on a Kubernetes/OpenShift cluster.
Also, the guide is adjusted to the current Jobtech platform with ArgoCD, Jobtech CI and Tekton.
Prerequisites is that [Tekton](https:/(/tekton.dev/)) is installed on the cluster,
in OpenShift this is the OpenShift Pipeline Operator.

## Download and login to Open Shift using OC (prerequisites)
Download the [Openshift CLI (oc)](https://docs.openshift.com/container-platform/4.7/cli_reference/openshift_cli/getting-started-cli.html)
Login to Openshift with oc  (first login to https://console-openshift-console.test.services.jtech.se and click on your logged in username in the upper right corner, and click "Copy login command")

Start by following this guide for setting up a new Open Shift project. Note that scraping-pipeline is an Open Shift project
deployed as an Tekton Pipeline.
Instead of a separate infra-repo as mentioned in the guide, add overlays that matches the new environment in this repo
under kustomize/overlays/
When deploying in the Jobtech environments there is also a corresponding scraping-pipeline-secrets repo.
`https://gitlab.com/arbetsformedlingen/documentation/-/blob/main/engineering/plattform/deploying-a-new-application.md`
### Apply secrets:
Apply empty secrets:
`oc apply -f example-secrets/empty-secret.yaml`
Apply secrets located in secrets repo (e.g. scraping-pipeline-secrets):
`oc apply -f example-secrets/[fil.yaml]`
### Add new steps (i.e. pipes):
Add new step in kustomize/base/scrape-pipeline.yaml
The steps are not executed by order in file but as specified in field runAfter.
The step (i.e. repo containing the code to execute) should have a tag that corresponds to the overlay for the current
environment (e.g. prod) and the commit hash that the tag points to will be executed.

### Start pipeline manually:
See  manual_runpipeline/README.md




